var mongo = require('mongodb');
var Db = mongo.Db;
var Connection = mongo.Connection;
var Server = mongo.Server;
var BSON = mongo.BSON;
var ObjectID = mongo.ObjectID;


var ImageProvider = function(host, port) {
	this.db = new Db('flizography-mongo', new Server(host, port, {auto_reconnect: true}, {}));
	this.db.open(function(){});
};


ImageProvider.prototype.getCollection = function(callback) {
	this.db.collection('images', function(error, image_collection) {
		if(error) callback(error);
		else callback(null, image_collection);
	});
};

ImageProvider.prototype.findAll = function(callback) {
	this.getCollection(function(error, image_collection){
		if(error) callback(error);
		else {
			image_collection.find().toArray(function(error, results){
				if(error) callback(error);
				else callback(null, results);
			});
		}
	});
};

ImageProvider.prototype.findById = function(id, callback){
	this.getCollection(function(error, image_collection){
		if(error) callback(error);
		else {
			image_collection.findOne({_id: image_collection.db.bson_serializer.ObjectID.createFromHexString(id)},
					function(error, results) {
				if(error) callback(error);
				else callback(null ,results);
			}
		}
	});
};

ImageProvider.prototype.findByUniqueId = function(id, callback){
	this.getCollection(function(error, image_collection){
		if(error) callback(error);
		else {
			image_collection.findOne({uniqueId: id},
					function(error, results) {
				if(error) callback(error);
				else callback(null ,results);
			}
		}
	});
};

ImageProvider.prototype.save = function(images, callback) {
	this.getCollection(function(error, image_collection){
		if(error) callback(error);
		else {
			if(typeof(images.length) == "undefined")
				images = [images];
			
			// Do some checking for missing values here 
			
			image_collection.insert(images, function(){
				callback(null, images);
			});
		}
	});
};

exports.ImageProvider = ImageProvider;