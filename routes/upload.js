/**
 * New node file
 */
var gm = require('gm');
var fs = require('fs');

// Just a temporary upload form for testing
exports.uploadForm  = function(req, res) {
	res.render("upload_form.jade");
};

exports.uploadImage = function(req, res, next) {
	
	console.log('\nuploaded %s to %s with size %s'
	        , req.files.image.name
	        , req.files.image.path
	        , req.files.image.size);
	
	var baseName = req.files.image.name.substring(0, req.files.image.name.lastIndexOf("."));
	var basePath = "./uploads/" +  baseName;
	gm(req.files.image.path)
		.autoOrient()
		.write(basePath + "_large.jpg", function(err){
		if(!err) {
			console.log("processed large thumbnail");
			gm(basePath + "_large.jpg")
			.resize(1000, 1000)
			.write(basePath + "_med.jpg", function (err) {
				if(!err) {
					console.log("processed medium thumbnail");
					gm(basePath + "_large.jpg")
					.resize(500, 500)
					.write(basePath + "_small.jpg", function (err) {
						if(err){
							res.send(500, { error: 'something blew up' });
						} else {
							console.log("processed small thumbnail");
							fs.unlink(req.files.image.path)
							saveImage(baseName, basePath);
						}
					});
				} else {
					res.send(500, { error: 'something blew up' });
				}
			});
		} else {
			res.send(500, { error: 'something blew up' });
		}
	});
	
	res.redirect('back');
};

var saveImage = function (baseName, basePath) {
	var fullData = fs.readFileSync(basePath + "_large.jpg");
	fs.unlink(basePath + "_large.jpg")
	var medData = fs.readFileSync(basePath + "_med.jpg");
	fs.unlink(basePath + "_med.jpg");
	var smallData = fs.readFileSync(basePath + "_small.jpg");
	fs.unlink(basePath + "_small.jpg");
	console.log(fullData);
	console.log(medData);
	console.log(smallData);
	
};